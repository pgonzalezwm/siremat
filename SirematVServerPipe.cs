﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Pipes;


namespace SirematDGTConnect
{
    class SirematVServerPipe
    {
        Thread ServerThread;
        RequestPlateQueue reqQueue;
        bool serverStarted = false;
        NamedPipeServerStream pipeStream;

        public void ThreadStartServer()
        {
            // Create a name pipe
            try
            {
                using (pipeStream = new NamedPipeServerStream("VehiclePServer", PipeDirection.InOut, 10, PipeTransmissionMode.Message,PipeOptions.Asynchronous))
                {
                    
                    pipeStream.WaitForConnection();                    
                    using (StreamReader sr = new StreamReader(pipeStream))
                    {
                        string request = "";
                        while ((request = sr.ReadLine()) != null && serverStarted)                        
                        {
                            reqQueue.addRequest(request);

                        }
                        pipeStream.Close();
                    }
                }
            }
            catch (Exception E)
            {
            }
            
               

        }

        public void StartServer()
        {
            serverStarted = true;

            reqQueue = new RequestPlateQueue();
            reqQueue.startRequestQueue();

            ServerThread = new System.Threading.Thread(ThreadStartServer);
            ServerThread.Start();

           
        }



        public void StopServer()
        {
         //   ServerThread.Abort();
            serverStarted = false;
           // if (pipeStream.IsConnected)
            
            reqQueue.stopRequestQueue();
            
        }

    }
}
