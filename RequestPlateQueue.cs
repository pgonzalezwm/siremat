﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;


namespace SirematDGTConnect
{
    class RequestPlateQueue
    {
        Queue requestQueue;
        Queue syncReqQueue;
        bool connected = false;
        Thread queueThread;

        public RequestPlateQueue()
        {
            requestQueue = new Queue();
            syncReqQueue = Queue.Synchronized(requestQueue);

        }

        public void startRequestQueue()
        {
            connected = true;
            queueThread = new Thread(doRequestsThread);
            queueThread.Start();
        }

        public void addRequest(String request)
        {
            syncReqQueue.Enqueue(request);
        }


        public void doRequestsThread()
        {
            while (connected)
            {
                if (syncReqQueue.Count > 0)
                {
                   
                    char[] separator = { ',' };
                    String request = (String)syncReqQueue.Dequeue();
                    String[] parts = request.Split(separator);
                    PlateRequestWS platRW = new PlateRequestWS();
                    platRW.getPlateInformation(parts);
                   
                }
                Thread.Sleep(500);
            }
        }

        public void stopRequestQueue()
        {
            connected = false;
            queueThread.Abort();
        }
    }
}
