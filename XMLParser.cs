﻿using System;
using System.Xml;


namespace SirematDGTConnect
{
    class XMLParser
    {
        private XmlDocument m_XmlDoc;
        private string m_XmlPath;

        public XMLParser()
        {
            this.m_XmlDoc = new XmlDocument();
        }

        public void LoadXmlString(string xmlT)
        {
            this.m_XmlDoc.LoadXml(xmlT);
        }

        public string GetCommandType()
        {
            return this.m_XmlDoc.DocumentElement.LocalName;
        }

        public string GetParamValue(string param)
        {
            return this.m_XmlDoc.DocumentElement[param].InnerText;
        }

        public bool LoadXMLFile(string path)
        {
            try
            {
                this.m_XmlPath = path;
                this.m_XmlDoc.Load(path);
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool SaveXMLFile()
        {
            try
            {
                this.m_XmlDoc.Save(this.m_XmlPath);
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public string GetValueElement(string parent, string element)
        {
            return ((XmlElement)((XmlElement)this.m_XmlDoc.GetElementsByTagName(parent)[0]).GetElementsByTagName(element)[0]).GetAttribute("value");
        }

        public void SetValueElement(string parent, string element, string value)
        {
            try
            {
                ((XmlElement)((XmlElement)this.m_XmlDoc.GetElementsByTagName(parent)[0]).GetElementsByTagName(element)[0]).SetAttribute("value", value);
            }
            catch (Exception ex)
            {
            }
        }

        public void InsertNewElement(string Element, string innerXML)
        {
            XmlElement element = this.m_XmlDoc.CreateElement(Element);
            element.InnerXml = innerXML;
            this.m_XmlDoc.DocumentElement.AppendChild((XmlNode)element);
        }

        public string GetElement(string head, string section, string entry)
        {
            return ((XmlElement)((XmlElement)this.m_XmlDoc.GetElementsByTagName(head)[0]).GetElementsByTagName(section)[0]).GetElementsByTagName(entry)[0].InnerText;
        }

        public void SetElement(string head, string section, string entry, string value)
        {
            try
            {
                ((XmlElement)((XmlElement)this.m_XmlDoc.GetElementsByTagName(head)[0]).GetElementsByTagName(section)[0]).GetElementsByTagName(entry)[0].InnerText = value;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        public int GetNodeCount(string head)
        {
            return this.m_XmlDoc.GetElementsByTagName(head)[0].ChildNodes.Count;
        }

        public XmlNodeList GetNodeList(string head, string tag)
        {
            return ((XmlElement)this.m_XmlDoc.GetElementsByTagName(head)[0]).GetElementsByTagName(tag);
        }

        public XmlNodeList GetNodeList(string head)
        {
            return this.m_XmlDoc.GetElementsByTagName(head);
        }

        public void DeleteAllNodes(string head)
        {
            this.m_XmlDoc.GetElementsByTagName(head)[0].RemoveAll();
        }

        public XmlDocument GetDocuXmlDocument()
        {
            return this.m_XmlDoc;
        }
    }
}
