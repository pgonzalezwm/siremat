﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace SirematDGTConnect
{
    class PlateRequestWS
    {
        VehicleWS.Auditoria auth;
        VehicleWS.SWConsultaVehiculosService swVeh;

        public PlateRequestWS()
        {
            swVeh = new VehicleWS.SWConsultaVehiculosService();
            swVeh.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            swVeh.consultaCompleted += new SirematDGTConnect.VehicleWS.consultaCompletedEventHandler(swVeh_consultaCompleted);
            swVeh.consultaMatriculaCompleted += new SirematDGTConnect.VehicleWS.consultaMatriculaCompletedEventHandler(swVeh_consultaMatriculaCompleted);
            auth = new VehicleWS.Auditoria();

            auth.aplicacionOrigen = "SIREMAT";
            auth.direccionIP = Utils.GetLocalIP();
            auth.usuario = "SIRDEE"+DateTime.Now.Ticks.ToString();
            auth.plantilla = "1";
        }

        void swVeh_consultaMatriculaCompleted(object sender, SirematDGTConnect.VehicleWS.consultaMatriculaCompletedEventArgs e)
        {
            try
            {
                String[] results = formatResponse(e.Result);
                for (int i = 0; i < results.Length; i++)
                {
                    SirematVClientPipe.addResponse(results[i]);
                }
            }
             catch(WebException we)
            {
                
                if (we.Response is HttpWebResponse)
                {
                   
                    HttpWebResponse response = (HttpWebResponse)we.Response;
                    Logger.log(DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + we.InnerException + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                    //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + we.InnerException + "\r\n");
                    SirematVClientPipe.addResponse("WS_ERROR:;"+response.StatusCode);
                }
            }
            catch (Exception E){
                SirematVClientPipe.addResponse("WS_ERROR:;");
                Logger.log(DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.InnerException + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.InnerException + "\r\n");
            }
        }

        void swVeh_consultaCompleted(object sender, SirematDGTConnect.VehicleWS.consultaCompletedEventArgs e)
        {
            try
            {
                String[] results = formatResponse(e.Result);
                for (int i = 0; i < results.Length; i++)
                {
                    SirematVClientPipe.addResponse(results[i]);
                }
            }
            catch(WebException we)
            {
                
                if (we.Response is HttpWebResponse)
                {
                   // SirematVClientPipe.addResponse("WS_ERROR:;");
                    HttpWebResponse response = (HttpWebResponse)we.Response;
                    Logger.log(DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + we.InnerException + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                    //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + we.InnerException+ "\r\n");
                    SirematVClientPipe.addResponse("WS_ERROR:;" + response.StatusCode);
                }
            }

            catch (Exception E){
                SirematVClientPipe.addResponse("WS_ERROR:;");
                Logger.log(DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.InnerException + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.InnerException + "\r\n");
                
            }
        }


        private String[] formatResponse(VehicleWS.respuestaVehiculosBean[] responses)
        {
            List<String> responseList = new List<string>();
            if (responses != null)
            {
                for (int i = 0; i < responses.Length; i++)
                {
                    String result = "";                    
                    result = responses[i].MenObj.ToUpper() + ":";
                    result += responses[i].Matricula.ToUpper() + ";";
                    if (responses[i].Bastidor != null)
                        result += responses[i].Bastidor.ToUpper();
                    result += ";";
                    if (responses[i].Marca != null)
                        result += responses[i].Marca.ToUpper();
                    result += ";";
                    if (responses[i].Modelo != null)
                        result += responses[0].Modelo.ToUpper();
                    result += ";";
                    if (responses[i].Dni != null)
                        result += responses[i].Dni.ToUpper();
                    result += ";";
                    if (responses[i].Apellidos != null)
                        result += responses[i].Apellidos.ToUpper();
                    result += ";";
                    if (responses[i].Nombre != null)
                        result += responses[i].Nombre.ToUpper();
                    result += ";";
                    if (responses[i].UtmX != null)
                        result += responses[i].UtmX.ToUpper();
                    result += ";";
                    if (responses[i].UtmY != null)
                        result += responses[i].UtmY.ToUpper();
                    result += ";";
                    if(responses[i].FchCtrl!=null)
                    {
                        result += responses[i].FchCtrl;
                    }
                    result += ";";
                    if(responses[i].HoraCtrl!=null)
                    {
                        result += responses[i].HoraCtrl;
                    }
                    result += ";";

                    responseList.Add(result);

                    Logger.log(DateTime.Now.ToString() + "->" + result + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                    //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->"+result+"\r\n");
                }
            }
            return responseList.ToArray();
        }

       // public String[] getPlateInformation(String[] request)
        public void getPlateInformation(String[] request)
        {

            if (request.Length < 4)
                return;

             //CHUSTA QUE HAY QUE QUITAR. SOLO PARA PRUEBAS FICOSA
            //***********************************************************************
            // Utils.flushDNS();
            //***********************************************************************
            VehicleWS.consultaVehiculosBean[] parameters = new VehicleWS.consultaVehiculosBean[request.Length - 3];

            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = new VehicleWS.consultaVehiculosBean();                
                parameters[i].UtmX = request[1];
                parameters[i].UtmY = request[2];
                if(request[0].Equals("2")) //Consulta Bastidor
                    parameters[i].Bastidor = request[i + 3];
                else
                    parameters[i].Matricula = request[i + 3]; //Consulta Matricula
                Logger.log(DateTime.Now.ToString() + "->" + "Consulta Matricula:" + parameters[i].Matricula + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
              //  System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Consulta Matricula:" + parameters[i].Matricula + "\r\n");
            }

            VehicleWS.consultaPeticionType consType = new VehicleWS.consultaPeticionType();
            consType.Auditoria = auth;
            consType.parameters = parameters;

           
            try
            {

                if (request[0].Equals("0"))
                {
                    //responses = swVeh.consultaMatricula(consType);
                    swVeh.consultaMatriculaAsync(consType);
                      
                }
                else
                {
                    swVeh.consultaAsync(consType);
                    //responses = swVeh.consulta(consType);
                }
            }
            catch (Exception E)
            {
                Logger.log(DateTime.Now.ToString() + "->" + "getPlateInformation:" + E.InnerException + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
                //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "getPlateInformation:" + E.InnerException + "\r\n");
            }

            //return formatResponse(responses);
        }
    }
}
