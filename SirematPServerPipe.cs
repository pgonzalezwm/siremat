﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Pipes;
using System.IO;
using System.Threading;

namespace SirematDGTConnect
{
    class SirematPServerPipe
    {
        Thread ServerThread;
        RequestPersonQueue reqQueue;
        bool serverStarted = false;
        NamedPipeServerStream pipeStream;
        
        public void ThreadStartServer()
        {
            try
            {
                using (pipeStream = new NamedPipeServerStream("PersonPServer", PipeDirection.InOut, 10, PipeTransmissionMode.Message, PipeOptions.Asynchronous))
                {
                     pipeStream.WaitForConnection();
                     Console.WriteLine("Conectado nuevo cliente....");
                     using (StreamReader sr = new StreamReader(pipeStream))
                     {
                         string request = "";
                         while ((request = sr.ReadLine()) != null && serverStarted)
                         {
                             reqQueue.addRequest(request);
                             //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + request + "\r\n");
                             //System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", "Peticion ->" + request + "\r\n");

                         }
                         //System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", "Servidor parado" + "\r\n");
                     }

                }
            }
            catch(Exception E){}
        }


        public void StartServer()
        {
            serverStarted = true;

            reqQueue = new RequestPersonQueue();
            reqQueue.startRequestQueue();


            ServerThread = new System.Threading.Thread(ThreadStartServer);
            ServerThread.Start();

            //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->GateWay Iniciado\r\n");
            
          //  System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", "PipeSever Arrancado" + "\r\n");
        }

        public void StopServer()
        {
            //ServerThread.Abort();
            serverStarted = false;
            pipeStream.Close();
            reqQueue.stopRequestQueue();
            //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->GateWay Detenido\r\n");
           // System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", "PipeSever Parado" + "\r\n");

        }
    }
}
