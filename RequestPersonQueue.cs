﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;

namespace SirematDGTConnect
{
    class RequestPersonQueue
    {
        Queue requestQueue;
        Queue syncReqQueue;
        bool connected = false;
        Thread queueThread;

        public RequestPersonQueue()
        {
            requestQueue = new Queue();
            syncReqQueue = Queue.Synchronized(requestQueue);

        }

        public void startRequestQueue()
        {
            connected = true;
            queueThread = new Thread(doRequestsThread);
            queueThread.Start();
        }

        public void addRequest(String request)
        {
            syncReqQueue.Enqueue(request);
        }

        public void doRequestsThread()
        {
            while (connected)
            {
                if (syncReqQueue.Count > 0)
                {
                    String result;
                    char[] separator = { ',' };
                    String request = (String)syncReqQueue.Dequeue();
                    String[] parts = request.Split(separator);
                    PersonRequestWS persWS = new PersonRequestWS();
                    persWS.getPersonInformation(parts);
                    //System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", request + "\r\n");
 /*                   result = persWS.getPersonInformation(parts);
                    if (result != null && result.Length > 0)
                    {
                        System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceIN.log", request + "\r\n");
                        System.IO.File.AppendAllText(@"\Temp\SIREMAT\SirematService\SirematServiceOUT.log", result + "\r\n");
                        SirematPClientPipe.sendResponse(result);
                    }*/


                }
                Thread.Sleep(500);
            }
        }

        public void stopRequestQueue()
        {
            connected = false;
            queueThread.Abort();
        }
    }
}
