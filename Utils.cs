﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SirematDGTConnect
{
    class Utils
    {

        public static string GetLocalIP()
        {
            string _IP = null;
            System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (System.Net.IPAddress _IPAddress in _IPHostEntry.AddressList)
            {
                if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    _IP = _IPAddress.ToString();
                }

            }
            return _IP;
        }

        public static string getTimestampToString()
        {
            string res = "";
            DateTime now = DateTime.Now;
            res = String.Format("{0:00}_{1:00}_{2:0000}",now.Day,now.Month,now.Year);
            return res;
        }

        public static void flushDNS()
        {

            try
            {
                Process ipconfig;
                ipconfig = new Process();
                ipconfig.StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "ipconfig.exe",
                    Arguments = "/flushdns"
                };
                ipconfig.Start();
                ipconfig.WaitForExit();
                ipconfig.Close();
            }
            catch (Exception E) { }
        }
    }
}

//Application.StartupPath