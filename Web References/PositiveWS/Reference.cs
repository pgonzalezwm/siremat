﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.34014.
// 
#pragma warning disable 1591

namespace SirematDGTConnect.PositiveWS {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ConsultaPosiblesPositivosPortBinding", Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public partial class ConsultaPosiblesPositivosService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback consultaPosiblesPositivosOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ConsultaPosiblesPositivosService() {
            this.Url = global::SirematDGTConnect.Properties.Settings.Default.SirematDGTConnect_PositiveWS_ConsultaPosiblesPositivosService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event consultaPosiblesPositivosCompletedEventHandler consultaPosiblesPositivosCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://posiblesPositivos.ws.personas.dgp.mir.es/", ResponseNamespace="http://posiblesPositivos.ws.personas.dgp.mir.es/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Respuesta consultaPosiblesPositivos([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] TipoOperacion operacion, [System.Xml.Serialization.XmlElementAttribute("parametros", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] Peticion[] parametros, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] Auditoria auditoria) {
            object[] results = this.Invoke("consultaPosiblesPositivos", new object[] {
                        operacion,
                        parametros,
                        auditoria});
            return ((Respuesta)(results[0]));
        }
        
        /// <remarks/>
        public void consultaPosiblesPositivosAsync(TipoOperacion operacion, Peticion[] parametros, Auditoria auditoria) {
            this.consultaPosiblesPositivosAsync(operacion, parametros, auditoria, null);
        }
        
        /// <remarks/>
        public void consultaPosiblesPositivosAsync(TipoOperacion operacion, Peticion[] parametros, Auditoria auditoria, object userState) {
            if ((this.consultaPosiblesPositivosOperationCompleted == null)) {
                this.consultaPosiblesPositivosOperationCompleted = new System.Threading.SendOrPostCallback(this.OnconsultaPosiblesPositivosOperationCompleted);
            }
            this.InvokeAsync("consultaPosiblesPositivos", new object[] {
                        operacion,
                        parametros,
                        auditoria}, this.consultaPosiblesPositivosOperationCompleted, userState);
        }
        
        private void OnconsultaPosiblesPositivosOperationCompleted(object arg) {
            if ((this.consultaPosiblesPositivosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.consultaPosiblesPositivosCompleted(this, new consultaPosiblesPositivosCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public enum TipoOperacion {
        
        /// <remarks/>
        OP1,
        
        /// <remarks/>
        OP2,
        
        /// <remarks/>
        OP3,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public partial class Peticion {
        
        private string nombreField;
        
        private string apellidosField;
        
        private string fechaNacimientoField;
        
        private bool posiblePositivoField;
        
        private bool posiblePositivoFieldSpecified;
        
        private bool esVigenteField;
        
        private bool esVigenteFieldSpecified;
        
        private Error[] erroresField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string apellidos {
            get {
                return this.apellidosField;
            }
            set {
                this.apellidosField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string fechaNacimiento {
            get {
                return this.fechaNacimientoField;
            }
            set {
                this.fechaNacimientoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public bool posiblePositivo {
            get {
                return this.posiblePositivoField;
            }
            set {
                this.posiblePositivoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool posiblePositivoSpecified {
            get {
                return this.posiblePositivoFieldSpecified;
            }
            set {
                this.posiblePositivoFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public bool esVigente {
            get {
                return this.esVigenteField;
            }
            set {
                this.esVigenteField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool esVigenteSpecified {
            get {
                return this.esVigenteFieldSpecified;
            }
            set {
                this.esVigenteFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("errores", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Error[] errores {
            get {
                return this.erroresField;
            }
            set {
                this.erroresField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public partial class Error {
        
        private string codigoField;
        
        private string descripcionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codigo {
            get {
                return this.codigoField;
            }
            set {
                this.codigoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public partial class Respuesta {
        
        private Peticion[] resultadoField;
        
        private Error errorField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultado", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Peticion[] resultado {
            get {
                return this.resultadoField;
            }
            set {
                this.resultadoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Error error {
            get {
                return this.errorField;
            }
            set {
                this.errorField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://posiblesPositivos.ws.personas.dgp.mir.es/")]
    public partial class Auditoria {
        
        private string usuarioField;
        
        private string plantillaField;
        
        private string direccionIPField;
        
        private string aplicacionOrigenField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string usuario {
            get {
                return this.usuarioField;
            }
            set {
                this.usuarioField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string plantilla {
            get {
                return this.plantillaField;
            }
            set {
                this.plantillaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string direccionIP {
            get {
                return this.direccionIPField;
            }
            set {
                this.direccionIPField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string aplicacionOrigen {
            get {
                return this.aplicacionOrigenField;
            }
            set {
                this.aplicacionOrigenField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void consultaPosiblesPositivosCompletedEventHandler(object sender, consultaPosiblesPositivosCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class consultaPosiblesPositivosCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal consultaPosiblesPositivosCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Respuesta Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Respuesta)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591