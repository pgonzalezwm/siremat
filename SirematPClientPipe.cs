﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Pipes;
using System.IO;
using System.Collections;
using System.Threading;

namespace SirematDGTConnect
{
    class SirematPClientPipe
    {
        Queue requestCQueue;
        static Queue syncReqPQueue;
        Thread ClientThread;
        SirematDGTConnectForm pParent;

        public SirematPClientPipe(SirematDGTConnectForm parent)
        {
            requestCQueue = new Queue();
            syncReqPQueue = Queue.Synchronized(requestCQueue);
            pParent = parent;
            ClientThread = new Thread(sendResponse);
            ClientThread.Start();
        }


        public  void sendResponse()
        {
            try
            {
                using (NamedPipeClientStream pipeStream = new NamedPipeClientStream(".", "PersonPClient"))
                {
                    while (!pipeStream.IsConnected && pParent.ServiceStarted)
                    {
                        try
                        {
                            pipeStream.Connect(3000);
                        }
                        catch (Exception E) { }
                    }

                    if (pipeStream.IsConnected)
                    {
                        using (StreamWriter sw = new StreamWriter(pipeStream))
                        {
                            sw.AutoFlush = true;
                            while (pParent.ServiceStarted)
                            {
                                if (syncReqPQueue.Count > 0)
                                {
                                    String response = (String)syncReqPQueue.Dequeue();
                                    sw.WriteLine(response);
                                }
                                Thread.Sleep(500);
                            }
                        }
                        pipeStream.Close();
                        pipeStream.Dispose();
                    }
                }
            }
            catch (Exception E) { }
        }

        public static void addRepsonse(String response)
        {
            syncReqPQueue.Enqueue(response);
        }

    }
}
