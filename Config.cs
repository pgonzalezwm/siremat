﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SirematDGTConnect
{
    class Config
    {
        private static XMLParser parser = new XMLParser();
        private static string user;

        public static string User
        {
            get { return user; }
            set { user = value; }
        }

        public static void LoadConfiguration()
        {
            Config.parser.LoadXMLFile("Config.xml");
            Config.user = Config.parser.GetElement("SirematDGT", "Auditoria", "User");

        }
    }
}
