﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SirematDGTConnect
{
    public partial class SirematDGTConnectForm : Form
    {
        bool serviceStarted = false;

        
        private SirematVServerPipe sirServV;
      //  private SirematPServerPipe sirServP;
        private SirematVClientPipe sirCliV;
       // private SirematPClientPipe sirCliP;

        private IContainer m_Container = null;
        public NotifyIcon m_NotifyIcon = null;
        private ContextMenu m_ContextMenu = null;

        public EventHandler evShowApp;
        public EventHandler evHideApp;
        public EventHandler evCloseApp;

        

        public SirematDGTConnectForm()
        {
            InitializeComponent();
            
         /*   try
            {
                Config.LoadConfiguration();
            }
            catch(Exception E)
            {
                MessageBox.Show(E.StackTrace); 
            }*/

            evShowApp = new EventHandler(evShowAppProc);
            evHideApp = new EventHandler(evHideAppProc);
            evCloseApp = new EventHandler(evCloseAppProc);

            sirServV = new SirematVServerPipe();
           // sirServP = new SirematPServerPipe();

            

            m_ContextMenu = new ContextMenu();
            m_ContextMenu.MenuItems.Add(new MenuItem("Restaurar", evShowApp));
            m_ContextMenu.MenuItems.Add(new MenuItem("Minimizar", evHideApp));
            m_ContextMenu.MenuItems.Add(new MenuItem("-"));
            m_ContextMenu.MenuItems.Add(new MenuItem("Cerrar", evCloseApp));
            


            Icon icon = SirematDGTConnect.Properties.Resources.diana;
            m_Container = new Container();
            m_NotifyIcon = new NotifyIcon(m_Container);
            m_NotifyIcon.ContextMenu = m_ContextMenu;
            m_NotifyIcon.Icon = icon;            
            m_NotifyIcon.Visible = true;

            if(!Directory.Exists(Application.StartupPath + "\\logs"))
            {
                Directory.CreateDirectory(Application.StartupPath + "\\logs");
            }
        }

        public bool ServiceStarted
        {
            get { return serviceStarted; }
            set { serviceStarted = value; }
        }


        void evShowAppProc(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Show();
        }

        void evCloseAppProc(object sender, EventArgs e)
        {
            if (serviceStarted)
            {
                sirServV.StopServer();
              //  sirServP.StopServer();
                lbServiceStatus.Text = "Servicio Detenido";
                lbServiceStatus.ForeColor = Color.DarkRed;
                serviceStarted = false;
            }

            Application.Exit();
        }

        void evHideAppProc(object sender, EventArgs e)
        {
          //  this.WindowState = FormWindowState.Minimized;
            this.Hide();
        }
        

        private void SirematDGTConnectForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serviceStarted)
            {
                sirServV.StopServer();
           //     sirServP.StopServer();
                lbServiceStatus.Text = "Servicio Detenido";
                lbServiceStatus.ForeColor = Color.DarkRed;
                serviceStarted = false;
            }

            Application.Exit();
        }

        private void SirematDGTConnectForm_Load(object sender, EventArgs e)
        {
           // Utils.flushDNS();
            sirServV.StartServer();
         //   sirServP.StartServer();
            lbServiceStatus.Text = "Servicio Arracando";
            lbServiceStatus.ForeColor = Color.DarkGreen;
            serviceStarted = true;
            sirCliV = new SirematVClientPipe(this);
          //  sirCliP = new SirematPClientPipe(this);
            this.Hide();
            
            
        }

        public static String getCurrentLogFile()
        {
            return Application.StartupPath + "\\logs\\" + Utils.getTimestampToString() + ".log";
        }
    }
}
