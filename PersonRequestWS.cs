﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SirematDGTConnect
{
    class PersonRequestWS
    {
        PositiveWS.Auditoria auth;
        PositiveWS.ConsultaPosiblesPositivosService swPer;

        public PersonRequestWS()
        {
            swPer = new PositiveWS.ConsultaPosiblesPositivosService();
            swPer.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            swPer.consultaPosiblesPositivosCompleted += new SirematDGTConnect.PositiveWS.consultaPosiblesPositivosCompletedEventHandler(swPer_consultaPosiblesPositivosCompleted);

            auth = new PositiveWS.Auditoria();
            auth.aplicacionOrigen = "SIREMAT";
            auth.direccionIP = Utils.GetLocalIP();
            auth.usuario = "SIRDEE001";
            auth.plantilla = "1";
        }

        void swPer_consultaPosiblesPositivosCompleted(object sender, SirematDGTConnect.PositiveWS.consultaPosiblesPositivosCompletedEventArgs e)
        {  
          
            String result = "";
            try
            {

                if (e.Result.resultado[0].posiblePositivo)
                {
                    result += "POSIBLE POSITIVO:";
                }
                else
                {
                    result += "SOLO DATOS DGT:";
                }



                result += ";;;;;";                
                result += e.Result.resultado[0].apellidos.ToUpper() + ",";
                result += e.Result.resultado[0].nombre.ToUpper() + ";";
                result += e.Result.resultado[0].fechaNacimiento;
                result += e.Result.resultado[0].esVigente.ToString();

                Logger.log(DateTime.Now.ToString() + "->" + result + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
               // System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + result + "\r\n");
                SirematPClientPipe.addRepsonse(result);
                // SirematPClientPipe.sendResponse(result)
            }
            catch (Exception E) {
                SirematPClientPipe.addRepsonse("WS_ERROR:;");
                Logger.log(DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.Message + "\r\n", SirematDGTConnectForm.getCurrentLogFile());
             //   System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Error en conexion a Escorial:" + E.Message + "\r\n");
            }
        }

        public void getPersonInformation(String[] request) //Nombre|Apellido1|Apellido2|Fecha Nacimiento
        {
            PositiveWS.Peticion req = new PositiveWS.Peticion();         
            req.apellidos = request[1] + " " + request[2];
            req.nombre = request[0];
            req.esVigente = true;
            req.posiblePositivo = true;
            req.fechaNacimiento = request[3]+"0000";
   

            PositiveWS.Peticion[] Requests = new PositiveWS.Peticion[1];
            Requests[0] = req;

            //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Consulta Persona:" +
            //                            swPer.ToString()+ "\r\n"+swPer.Container+"\r\n"+swPer.SoapVersion+"\r\n"+swPer.Url);
            


            swPer.consultaPosiblesPositivosAsync(PositiveWS.TipoOperacion.OP1, Requests, auth);
            //System.IO.File.AppendAllText(SirematDGTConnectForm.getCurrentLogFile(), DateTime.Now.ToString() + "->" + "Consulta Persona:" + 
            //                             req.nombre+","+req.apellidos+","+req.fechaNacimiento+ "\r\n");
            
            //PositiveWS.Respuesta response = swPer.consultaPosiblesPositivos(PositiveWS.TipoOperacion.OP1, Requests, auth);

            /*String res = "";

            if (response.resultado[0].posiblePositivo)
            {
                res += "POSIBLE POSITIVO";
            }
            else
            {
                res += "SOLO DATOS DGT";
            }

            res += ";;;;;";
            res += response.resultado[0].apellidos.ToUpper() + ",";
            res += response.resultado[0].nombre.ToUpper() + ";";
            res += response.resultado[0].fechaNacimiento;


            return res;*/
        }



    }
}
