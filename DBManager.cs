﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SirematDGTConnect.VehicleWS;
using MySql.Data.MySqlClient;

namespace SirematDGTConnect
{
    class DBManager
    {

       
        static string DbString = "server=localhost;user id=root; password=1234; database=siremat; pooling=false";
        public static void InsertResponseInDB(respuestaVehiculosBean response)
        {
            int idOwner = -1;
            int idVehicle = -1;
            try
            {
                using (MySqlConnection mComm = new MySqlConnection(DbString))
                {
                    mComm.Open();
                    using (MySqlCommand ObjCmd = new MySqlCommand("InsertOwner", mComm))
                    {
                        ObjCmd.CommandType = System.Data.CommandType.StoredProcedure;
                        ObjCmd.Parameters.Add("DNI_", MySqlDbType.String).Value = response.Dni.ToUpper();
                        if (response.Nombre != null)
                            ObjCmd.Parameters.Add("NAM", MySqlDbType.String).Value = response.Nombre;
                        else
                            ObjCmd.Parameters.Add("NAM", MySqlDbType.String).Value = " ";
                        if (response.Apellidos != null)
                        {
                            char[] sep = { ' ' };
                            String[] apells = response.Apellidos.Split(sep);
                            if (apells[0] != null)
                                ObjCmd.Parameters.Add("APELL1", MySqlDbType.String).Value = apells[0];
                            else
                                ObjCmd.Parameters.Add("APELL1", MySqlDbType.String).Value = " ";
                            if (apells[1] != null)
                                ObjCmd.Parameters.Add("APELL2", MySqlDbType.String).Value = apells[1];
                            else
                                ObjCmd.Parameters.Add("APELL1", MySqlDbType.String).Value = " ";
                        }
                        else
                        {
                            ObjCmd.Parameters.Add("APELL1", MySqlDbType.String).Value = " ";
                            ObjCmd.Parameters.Add("APELL2", MySqlDbType.String).Value = " ";
                        }

                        MySqlParameter Out = new MySqlParameter("RES", MySqlDbType.Int32);
                        Out.Direction = System.Data.ParameterDirection.Output;
                        ObjCmd.Parameters.Add(Out);
                        ObjCmd.ExecuteNonQuery();

                        idOwner = Convert.ToInt32(ObjCmd.Parameters["RES"].Value.ToString());
                    }
                    if (idOwner != -1)
                    {
                        using (MySqlCommand ObjCmd = new MySqlCommand("InsertVehicle", mComm))
                        {
                            ObjCmd.CommandType = System.Data.CommandType.StoredProcedure;
                            ObjCmd.Parameters.Add("MAT", MySqlDbType.String).Value = response.Matricula;
                            if (response.Bastidor != null)
                                ObjCmd.Parameters.Add("BAST", MySqlDbType.String).Value = response.Bastidor;
                            else
                                ObjCmd.Parameters.Add("BAST", MySqlDbType.String).Value = " ";
                            if (response.Marca != null)
                                ObjCmd.Parameters.Add("MARC", MySqlDbType.String).Value = response.Marca;
                            else
                                ObjCmd.Parameters.Add("MARC", MySqlDbType.String).Value = " ";
                            if (response.Modelo != null)
                                ObjCmd.Parameters.Add("MODEL", MySqlDbType.String).Value = response.Modelo;
                            else
                                ObjCmd.Parameters.Add("MODEL", MySqlDbType.String).Value = " ";
                            if (response.MenObj.ToUpper().Equals("POSIBLE POSITIVO"))
                                ObjCmd.Parameters.Add("REQ", MySqlDbType.Int32).Value = 1;
                            else
                                ObjCmd.Parameters.Add("REQ", MySqlDbType.Int32).Value = 0;
                            ObjCmd.Parameters.Add("IDOWNER", MySqlDbType.Int32).Value = idOwner;

                            MySqlParameter Out = new MySqlParameter("RES", MySqlDbType.Int32);
                            Out.Direction = System.Data.ParameterDirection.Output;
                            ObjCmd.Parameters.Add(Out);
                            ObjCmd.ExecuteNonQuery();
                            idVehicle = Convert.ToInt32(ObjCmd.Parameters["RES"].Value.ToString());
                        }
                    }

                }
            }
            catch (Exception E) { }
        }
    }
}
