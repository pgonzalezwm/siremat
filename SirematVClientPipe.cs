﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Pipes;
using System.IO;
using System.Collections;
using System.Threading;

namespace SirematDGTConnect
{
    class SirematVClientPipe
    {
        Queue requestCQueue;
        static Queue syncReqCQueue;
        Thread ClientThread;
        SirematDGTConnectForm pParent;

        public SirematVClientPipe(SirematDGTConnectForm parent)
        {
            requestCQueue = new Queue();
            syncReqCQueue = Queue.Synchronized(requestCQueue);
            pParent = parent;
            ClientThread = new Thread(sendResponse);
            ClientThread.Start();
        }



        public void sendResponse()
        {
            try
            {
                using (NamedPipeClientStream pipeStream = new NamedPipeClientStream(".","VehiclePClient"))
                {
                    while (!pipeStream.IsConnected && pParent.ServiceStarted)
                    {
                        try
                        {
                            pipeStream.Connect(3000);
                        }
                        catch (Exception E) { }
                    }

                    if (pipeStream.IsConnected)
                    {
                        using (StreamWriter sw = new StreamWriter(pipeStream))
                        {
                            sw.AutoFlush = true;
                            while (pParent.ServiceStarted)
                            {
                                if (syncReqCQueue.Count > 0)
                                {
                                    String response = (String)syncReqCQueue.Dequeue();
                                    sw.WriteLine(response);
                                }
                                Thread.Sleep(500);
                            }
                        }
                        pipeStream.Close();
                        pipeStream.Dispose();
                    }
                }
            }
            catch (Exception E) { }
        }

        public static void addResponse(String response)
        {
            syncReqCQueue.Enqueue(response);
        }

    }
}
